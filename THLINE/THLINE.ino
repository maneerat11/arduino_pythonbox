#include <Scheduler.h>
#include <TridentTD_LineNotify.h>
#include <LiquidCrystal_I2C.h>
#include "DHT.h"

//DHT11
#define DHTPIN D6
#define DHTTYPE DHT11
#define SW D4

//Connect WiFi
//SSID
#define SSID "iPhoneP'pae"
//Password
#define PASSWORD "Pae290741"

//LINE TOKEN
#define LINE_TOKEN "EoEsFlWHogGKL0gXo86NTSGHiOpUywVaDZx8H4SoRNH"

DHT dht(DHTPIN, DHTTYPE);
LiquidCrystal_I2C lcd(0x27, 16, 2);

int analogPin = A0; //define analogpin
int analog_val = 0;

bool sendval = false;
bool sendfail = false;
bool donesend = false;

//For DHT11
class FirstTask : public Task {
protected:
    void setup() {

    }

    void loop() {
        String dht11_val = "";
        float h = dht.readHumidity();
        float t = dht.readTemperature();

        if (isnan(h) || isnan(t)) {
            Serial.println("Failed to read from DHT sensor!");
        }
        dht11_val = dht11_val + "ขณะนี้อุณหภูมิ "+t+"°C"+ " ความชื้น "+h+"%";
        Serial.println(dht11_val);
        LINE.notify(dht11_val);
        delay(60000);
    }

private:
    uint8_t state;
} first_task;

//For RainDrop
class SecondTask : public Task {
protected:
    void setup() {
       pinMode(SW,INPUT);
       Serial.begin(9600); 
    }

    void loop() {
        analog_val = analogRead(analogPin);
        Serial.print("analog val = ");
        Serial.println(analog_val); 

        if (digitalRead(SW) == HIGH){
          while(digitalRead(SW) == HIGH) delay(10);
          LINE.notify("ตอนนี้มีน้ำหกหรืองูปัสสาวะนะจ๊ะ");
        }
        delay(10);
  }
    
private:
    uint8_t state;
} second_task;

//For LCD
class ThirdTask : public Task {
protected:
    void setup() {
        
    }

    void loop() {
        float h = dht.readHumidity();
        float t = dht.readTemperature();

        if (isnan(h) || isnan(t)) {
            Serial.println(F("Failed to read from DHT sensor!"));
            return;
        }
        lcd.setCursor(0, 0);
        lcd.print("Temp:     ");
        lcd.setCursor(4, 0);
        lcd.print(t);
        lcd.setCursor(9, 0);
        lcd.print("C");
        lcd.setCursor(0, 1);
        lcd.print("Hum:     ");
        lcd.setCursor(4, 1);
        lcd.print(h);
        lcd.setCursor(9, 1);
        lcd.print("%");
        delay(2000);
    }
    
private:
    uint8_t state;
} third_task;

void setup() {
    Serial.begin(9600);

    dht.begin();
    lcd.begin();

    Serial.println();
    Serial.println(LINE.getVersion());
    WiFi.begin(SSID, PASSWORD);
    Serial.printf("WiFi connecting to %s\n", SSID);

    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(2000);
    }

    Serial.printf("\nWiFi connected\nIP : ");
    Serial.println(WiFi.localIP());
    LINE.setToken(LINE_TOKEN);
    LINE.notify("เซนเซอร์วัดอุณหภูมิ ความชื้นและเซนเซอร์ตรวจจับน้ำหกหรืองูปัสสาวะ เริ่มทำงานแล้ว");

    Scheduler.start(&first_task);
    Scheduler.start(&second_task);
    Scheduler.start(&third_task);
    Scheduler.begin();
}

void loop() {

}
