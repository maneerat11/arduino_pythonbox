# Project
This project is on Dev. 🎃

## Hardware
1. ESP8266
2. DHT11
3. Raindrop senser
4. LCD

##  Library
1. [DHT11](https://github.com/adafruit/DHT-sensor-library)
2. [LINE Notify](https://github.com/TridentTD/TridentTD_LineNotify)
3. [ESP8266Scheduler](https://github.com/nrwiersma/ESP8266Scheduler) **or** Search on manage libraries **"ESP8266Scheduler"**
4. [LiquidCrystal](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library) **or** Search on manage libraries **"LiquidCrystal"**