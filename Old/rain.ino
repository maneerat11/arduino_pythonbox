#include <TridentTD_LineNotify.h>


#define SSID "NO OK" // แก้ ชื่อ ssid ของ wifi 
#define PASSWORD "nook247538" // แก้ รหัสผ่าน wifi
#define LINE_TOKEN "cLfVuNzPxZhU2l9Yyx8pvbAtrrAQldmxpxmdHAaSYTh" // แก้ Line Token

int analogPin = A0; //ประกาศตัวแปร ให้ analogPin 
int val = 0;
void setup() {
  pinMode(ledPin, OUTPUT);  // sets the pin as output
  Serial.begin(9600);
  Serial.println();
  Serial.println(LINE.getVersion());
  WiFi.begin(SSID, PASSWORD);
  Serial.printf("WiFi connecting to %s\n", SSID);
  while (WiFi.status() != WL_CONNECTED) {
   Serial.print(".");
    delay(2000);
  }
  Serial.printf("\nWiFi connected\nIP : ");
  Serial.println(WiFi.localIP());
  LINE.setToken(LINE_TOKEN);
  LINE.notify("เซนเซอร์ตรวจจับน้ำหกหรืองูปัสวะ เริ่มทำงานแล้ว");
}

void loop() {
  val = analogRead(analogPin); //อ่านค่าสัญญาณ analog 
  Serial.print("val = "); // พิมพ์ข้อมความส่งเข้าคอมพิวเตอร์ "val = "
  Serial.println(val); // พิมพ์ค่าของตัวแปร val
  if (val < 1024) { // สามารถกำหนดปรับค่าได้ตามสถานที่ต่างๆ
      LINE.notify("ตอนนี้มีน้ำหกหรืองูปัสวะนะจ๊ะ");
      delay(70000);
  }
  else {
    Serial.println(val);
  }
}
